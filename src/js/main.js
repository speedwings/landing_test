import Vue from 'vue/dist/vue'
import VeeValidate from 'vee-validate'
import NewsletterForm from './components/NewsletterForm'
import PushSubscription from './components/PushSubscription'
import ProductsGrid from "./components/ProductsGrid";

Vue.use(VeeValidate, {
    locale: 'it',
    events: 'blur',
    dictionary: {
        it: {
            custom: {
                email: {
                    required: 'Il campo email è obbligatorio.',
                    email: 'Inserire un indirizzo email valido.',
                },
                privacy: {
                    required: 'L\'approvazione della privacy policy è obbligatoria.'
                }
            }
        }
    }
})

Vue.component('newsletter-form', NewsletterForm)
Vue.component('push-subscription', PushSubscription)
Vue.component('products-grid', ProductsGrid)

const app = new Vue({
    el: '#app',
    data: {
        tabIndex: 0
    },
    computed: {
        supportsPush () {
            return !!(window.Notification && navigator.serviceWorker);
        }
    }
})
