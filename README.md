# Landing Test
## Install
To install node packages, execute:

``` npm install ```

## Development
Webpack dev server runs on port 8811 with HMR. To boot dev server, run script *dev* with:

``` npm run dev ```

## Deploy
Run *prod* script to build the project in *dist* folder, with:

``` npm run dev ```

### References
- [*Ref. repository*](https://github.com/llenzi/landingexample02)
- [*API*](https://jsonplaceholder.typicode.com/photos)
